## The other day I might consider learning autoconf. In the meantime I
## hope this is trivial enough to be installed without. If someone
## feels like sending me an autoconf-script for cpipe, I'll use it.
##
## $Revision: 1.5 $, $Date: 2003/07/22 08:15:11 $
########################################################################

prefix=/usr
exec_prefix=$(prefix)

## This is where the executable will be installed
BINDIR=$(exec_prefix)/bin

## Here we install the manual page
MANDIR=$(prefix)/man/man1

## Your favorite compiler flags.
#CFLAGS ?= -O2 -W -Wall -pedantic

########################################################################
cpipe: cpipe.o cmdline.o
	$(CC) $(LDFLAGS) -o $@ cpipe.o cmdline.o -lm

cpipe.o: cpipe.c cmdline.h

cmdline.o: cmdline.c cmdline.h

clean:
	-rm cmdline.o cpipe.o cpipe


install: cpipe cpipe.1
	mkdir -p $(BINDIR) $(MANDIR)
	cp cpipe $(BINDIR); chmod 755 $(BINDIR)/cpipe
	cp cpipe.1 $(MANDIR); chmod 744 $(MANDIR)/cpipe.1

